module Main exposing (Model(..), Msg(..), init, main, update, view)

import App.Init as MainInit
import App.Model as MainModel
import App.Update as MainUpdate
import App.View as MainView
import Browser exposing (Document)
import Html exposing (Html)
import Html.Attributes as Attributes
import Http.Progress exposing (Progress(..))



---- MODEL ----


type Model
    = MainPage MainModel.Model


init : ( Model, Cmd Msg )
init =
    let
        ( initModel, initCmd ) =
            MainInit.init
    in
    ( MainPage initModel, initCmd |> Cmd.map MainMsg )



---- UPDATE ----


type Msg
    = MainMsg MainUpdate.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model ) of
        ( MainMsg mainMsg, MainPage mainModel ) ->
            let
                ( updatedModel, updatedMsg ) =
                    MainUpdate.update mainMsg mainModel
            in
            ( MainPage updatedModel, Cmd.map MainMsg updatedMsg )



---- VIEW ----


view : Model -> Document Msg
view model =
    { title = "Østkantkonferansen"
    , body = [ viewBody model ]
    }


viewBody : Model -> Html Msg
viewBody model =
    Html.div
        [ Attributes.class "page" ]
        [ case model of
            MainPage mainModel ->
                MainView.view mainModel |> Html.map MainMsg
        ]



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.document
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = always Sub.none
        }
