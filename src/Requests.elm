module Requests exposing (getImage, getInformation, getProgram)

import Http
import RemoteData exposing (WebData)
import Types.Information as Information exposing (Information)
import Types.Program.AssetId as AssetId exposing (AssetId)
import Types.Program.Image as Image exposing (Image)
import Types.Program.Program as Program exposing (Program)
import Types.Program.ProgramEntry exposing (ProgramEntry(..))


getProgram : (WebData Program.Program -> msg) -> Cmd msg
getProgram message =
    RemoteData.sendRequest programRequest
        |> Cmd.map message


getInformation : (WebData Information -> msg) -> Cmd msg
getInformation message =
    RemoteData.sendRequest informationRequest
        |> Cmd.map message


getImage : (( AssetId, WebData Image ) -> msg) -> ProgramEntry -> Cmd msg
getImage message (ProgramEntry entry) =
    Maybe.map (getAsset message) entry.imageId
        |> Maybe.withDefault Cmd.none


getAsset : (( AssetId, WebData Image ) -> msg) -> AssetId -> Cmd msg
getAsset message assetId =
    RemoteData.sendRequest (assetRequest assetId)
        |> Cmd.map (\imageData -> ( assetId, imageData ))
        |> Cmd.map message


programRequest : Http.Request Program.Program
programRequest =
    Http.get
        (baseEntryRequestUrl "programoppfringer")
        Program.decoder


informationRequest : Http.Request Information
informationRequest =
    Http.get
        (baseEntryRequestUrl "informasjonsbolker")
        Information.decoder


assetRequest : AssetId -> Http.Request Image
assetRequest assetId =
    Http.get
        (baseApiUrl ++ "/spaces/" ++ space ++ "/environments/" ++ environment ++ "/assets/" ++ AssetId.id assetId ++ "?access_token=" ++ accessToken)
        Image.decoder


baseEntryRequestUrl : String -> String
baseEntryRequestUrl contentType =
    baseApiUrl ++ "/spaces/" ++ space ++ "/environments/" ++ environment ++ "/entries?access_token=" ++ accessToken ++ "&content_type=" ++ contentType


baseApiUrl : String
baseApiUrl =
    "https://cdn.contentful.com"


space : String
space =
    "pmkstegwyyrt"


environment : String
environment =
    "master"


accessToken : String
accessToken =
    "1a52f1597a66903c16478874ea3f05684d58aa9864c2bcaa4eb198e7552c30b2"
