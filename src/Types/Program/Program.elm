module Types.Program.Program exposing (Program(..), addImage, decoder, map, sort)

import Json.Decode as Decode exposing (Decoder)
import RemoteData exposing (WebData)
import Types.Program.AssetId exposing (AssetId)
import Types.Program.Image exposing (Image)
import Types.Program.ProgramEntry as ProgramEntry exposing (ProgramEntry)
import Types.Program.Timeslot as Timeslot


type Program
    = Program (List ProgramEntry)


decoder : Decoder Program
decoder =
    ProgramEntry.decoder
        |> Decode.field "fields"
        |> Decode.list
        |> Decode.field "items"
        |> Decode.map Program


addImage : ( AssetId, WebData Image ) -> Program -> Program
addImage image (Program entries) =
    Program
        (List.map
            (ProgramEntry.updateImage image)
            entries
        )


map : (ProgramEntry -> a) -> Program -> List a
map f (Program entries) =
    List.map
        f
        entries


sort : Program -> Program
sort (Program entries) =
    Program
        (List.sortWith
            (\first second -> Timeslot.compare (ProgramEntry.beginTime first) (ProgramEntry.beginTime second))
            entries
        )
