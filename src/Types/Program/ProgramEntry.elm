module Types.Program.ProgramEntry exposing
    ( ProgramEntry(..)
    , beginTime
    , decoder
    , description
    , endTime
    , entryType
    , image
    , imageId
    , presenter
    , summary
    , title
    , updateImage
    )

import Json.Decode as Decode exposing (Decoder)
import RemoteData exposing (WebData)
import Types.Program.AssetId as AssetId exposing (AssetId)
import Types.Program.Description as Description exposing (Description)
import Types.Program.EntryType as EntryType exposing (EntryType)
import Types.Program.Image exposing (Image)
import Types.Program.Presenter as Presenter exposing (Presenter)
import Types.Program.Summary as Summary exposing (Summary)
import Types.Program.Timeslot as Timeslot exposing (Timeslot)
import Types.Program.Title as Title exposing (Title)


type ProgramEntry
    = ProgramEntry
        { title : Title
        , presenter : Maybe Presenter
        , summary : Maybe Summary
        , description : Maybe Description
        , beginTime : Timeslot
        , endTime : Timeslot
        , entryType : EntryType
        , imageId : Maybe AssetId
        , image : WebData Image
        }


updateImage : ( AssetId, WebData Image ) -> ProgramEntry -> ProgramEntry
updateImage ( id, imageData ) ((ProgramEntry entry) as programEntry) =
    Maybe.map
        (\imgId ->
            if imgId == id then
                ProgramEntry { entry | image = imageData }

            else
                programEntry
        )
        entry.imageId
        |> Maybe.withDefault programEntry


decoder : Decoder ProgramEntry
decoder =
    Decode.map8
        init
        (Decode.field "tittel" Title.decoder)
        (Decode.field "hvem" Presenter.decoder |> Decode.maybe)
        (Decode.field "oppsummering" Summary.decoder |> Decode.maybe)
        (Decode.field "beskrivelse" Description.decoder |> Decode.maybe)
        (Timeslot.decoder |> Decode.field "starttidspunkt")
        (Timeslot.decoder |> Decode.field "sluttidspunkt")
        (EntryType.decoder |> Decode.field "programtype")
        (Decode.at [ "bilde", "sys", "id" ] AssetId.decoder |> Decode.maybe)


init : Title -> Maybe Presenter -> Maybe Summary -> Maybe Description -> Timeslot -> Timeslot -> EntryType -> Maybe AssetId -> ProgramEntry
init t p s d bt et eType imgId =
    ProgramEntry
        { title = t
        , presenter = p
        , summary = s
        , description = d
        , beginTime = bt
        , endTime = et
        , entryType = eType
        , imageId = imgId
        , image = RemoteData.NotAsked
        }


title : ProgramEntry -> Title
title (ProgramEntry entry) =
    entry.title


presenter : ProgramEntry -> Maybe Presenter
presenter (ProgramEntry entry) =
    entry.presenter


summary : ProgramEntry -> Maybe Summary
summary (ProgramEntry entry) =
    entry.summary


description : ProgramEntry -> Maybe Description
description (ProgramEntry entry) =
    entry.description


beginTime : ProgramEntry -> Timeslot
beginTime (ProgramEntry entry) =
    entry.beginTime


endTime : ProgramEntry -> Timeslot
endTime (ProgramEntry entry) =
    entry.endTime


entryType : ProgramEntry -> EntryType
entryType (ProgramEntry entry) =
    entry.entryType


imageId : ProgramEntry -> Maybe AssetId
imageId (ProgramEntry entry) =
    entry.imageId


image : ProgramEntry -> WebData Image
image (ProgramEntry entry) =
    entry.image
