module Types.Program.Presenter exposing (Presenter, decoder, toString)

import Json.Decode as Decode exposing (Decoder)


type Presenter
    = Presenter String


decoder : Decoder Presenter
decoder =
    Decode.string |> Decode.map Presenter


toString : Presenter -> String
toString (Presenter presenter) =
    presenter
