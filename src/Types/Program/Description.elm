module Types.Program.Description exposing (Description, decoder)

import Json.Decode as Decode exposing (Decoder)


type Description
    = Description String


decoder : Decoder Description
decoder =
    Decode.string |> Decode.map Description
