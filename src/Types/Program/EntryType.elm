module Types.Program.EntryType exposing (EntryType(..), decoder)

import Json.Decode as Decode exposing (Decoder)


type EntryType
    = Foredrag
    | Workshop
    | Panel
    | Innledning
    | Praktisk
    | Kultur
    | Annet


decoder : Decoder EntryType
decoder =
    Decode.string |> Decode.map fromString


fromString : String -> EntryType
fromString typeString =
    case typeString of
        "Foredrag" ->
            Foredrag

        "Workshop" ->
            Workshop

        "Panel" ->
            Panel

        "Innledning" ->
            Innledning

        "Informasjon/Praktisk" ->
            Praktisk

        "Kulturinnslag" ->
            Kultur

        _ ->
            Annet
