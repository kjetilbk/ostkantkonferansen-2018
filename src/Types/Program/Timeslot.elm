module Types.Program.Timeslot exposing (Timeslot, compare, decoder, icon, toString)

import Json.Decode as Decode exposing (Decoder)
import Time.DateTime as DateTime exposing (DateTime)
import Time.Iso8601 as Iso8601


type Timeslot
    = Timeslot DateTime


decoder : Decoder Timeslot
decoder =
    Decode.string
        |> Decode.map (\str -> str ++ ":00Z")
        |> Decode.map Iso8601.toDateTime
        |> Decode.map (Result.mapError (\_ -> ""))
        |> Decode.andThen
            (\parseResult ->
                case parseResult of
                    Ok dateTime ->
                        Decode.succeed (Timeslot dateTime)

                    Err parseError ->
                        Decode.fail parseError
            )


toString : Timeslot -> String
toString ((Timeslot dateTime) as timeslot) =
    (DateTime.hour dateTime |> String.fromInt)
        ++ ":"
        ++ toMinutes timeslot


toMinutes : Timeslot -> String
toMinutes (Timeslot dateTime) =
    let
        minutes =
            DateTime.minute dateTime |> String.fromInt
    in
    if String.length minutes == 1 then
        "0" ++ minutes

    else
        minutes


icon : Timeslot -> String
icon (Timeslot dateTime) =
    case modBy 12 (DateTime.hour dateTime) of
        1 ->
            "🕐"

        2 ->
            "🕑"

        3 ->
            "🕒"

        4 ->
            "🕓"

        5 ->
            "🕔"

        6 ->
            "🕕"

        7 ->
            "🕖"

        8 ->
            "🕗"

        9 ->
            "🕘"

        10 ->
            "🕙"

        11 ->
            "🕚"

        12 ->
            "🕛"

        _ ->
            "🕛"


compare : Timeslot -> Timeslot -> Order
compare (Timeslot first) (Timeslot second) =
    DateTime.compare first second
