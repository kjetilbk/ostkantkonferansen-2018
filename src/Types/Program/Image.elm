module Types.Program.Image exposing (Image, decoder, title, url)

import Json.Decode as Decode exposing (Decoder)
import Types.Program.AssetId as AssetId exposing (AssetId)


type Image
    = Image
        { title : String
        , url : String
        , id : AssetId
        }


decoder : Decoder Image
decoder =
    Decode.map3
        init
        (Decode.at [ "fields", "title" ] Decode.string)
        (Decode.at [ "fields", "file", "url" ] Decode.string)
        (Decode.at [ "sys", "id" ] AssetId.decoder)


init : String -> String -> AssetId -> Image
init t urlString id =
    Image { title = t, url = urlString, id = id }


url : Image -> String
url (Image image) =
    image.url


title : Image -> String
title (Image image) =
    image.title
