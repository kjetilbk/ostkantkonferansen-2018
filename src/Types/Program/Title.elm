module Types.Program.Title exposing (Title, decoder, toString)

import Json.Decode as Decode exposing (Decoder)


type Title
    = Title String


decoder : Decoder Title
decoder =
    Decode.string |> Decode.map Title


toString : Title -> String
toString (Title programTitle) =
    programTitle
