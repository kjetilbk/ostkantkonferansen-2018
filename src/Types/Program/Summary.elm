module Types.Program.Summary exposing (Summary, decoder, toString)

import Json.Decode as Decode exposing (Decoder)


type Summary
    = Summary String


decoder : Decoder Summary
decoder =
    Decode.string |> Decode.map Summary


toString : Summary -> String
toString (Summary summary) =
    summary
