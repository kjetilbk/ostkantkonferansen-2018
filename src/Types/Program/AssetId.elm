module Types.Program.AssetId exposing (AssetId, decoder, id)

import Json.Decode as Decode exposing (Decoder)


type AssetId
    = AssetId String


id : AssetId -> String
id (AssetId assetId) =
    assetId


decoder : Decoder AssetId
decoder =
    Decode.string |> Decode.map AssetId
