module Types.Information exposing
    ( Information(..)
    , InformationEntry
    , content
    , decoder
    , entries
    )

import Json.Decode as Decode exposing (Decoder)
import Types.Title as Title exposing (Title)


type Information
    = Information (List InformationEntry)


type alias InformationEntry =
    { title : Title
    , row1 : Maybe String
    , row2 : Maybe String
    , row3 : Maybe String
    , row4 : Maybe String
    }


decoder : Decoder Information
decoder =
    informationEntryDecoder
        |> Decode.field "fields"
        |> Decode.list
        |> Decode.field "items"
        |> Decode.map Information


informationEntryDecoder : Decoder InformationEntry
informationEntryDecoder =
    Decode.map5
        InformationEntry
        (Decode.field "overskrift" Title.decoder)
        (Decode.field "rad1" Decode.string |> Decode.maybe)
        (Decode.field "rad2" Decode.string |> Decode.maybe)
        (Decode.field "rad3" Decode.string |> Decode.maybe)
        (Decode.field "rad4" Decode.string |> Decode.maybe)


entries : Information -> List InformationEntry
entries (Information es) =
    es


content : InformationEntry -> String
content entry =
    (Maybe.withDefault "\n" entry.row1
        ++ Maybe.withDefault "\n" entry.row2
        ++ Maybe.withDefault "\n" entry.row3
        ++ Maybe.withDefault "\n" entry.row4
    )
        |> String.trim
