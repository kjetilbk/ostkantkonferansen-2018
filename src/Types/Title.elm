module Types.Title exposing (Title, decoder, title, toString)

import Json.Decode as Decode exposing (Decoder)


type Title
    = Title String


toString : Title -> String
toString (Title programTitle) =
    programTitle


decoder : Decoder Title
decoder =
    Decode.string |> Decode.map Title


title : String -> Title
title titleString =
    Title titleString
