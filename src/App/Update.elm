module App.Update exposing (Msg(..), update)

import App.Model exposing (Model)
import RemoteData exposing (WebData)
import Requests
import Types.Information exposing (Information)
import Types.Program.AssetId exposing (AssetId)
import Types.Program.Image exposing (Image)
import Types.Program.Program as Program exposing (Program(..))


type Msg
    = ProgramRetrieved (WebData Program.Program)
    | ProgramImagesRetrieved ( AssetId, WebData Image )
    | InformationRetrieved (WebData Information)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ProgramRetrieved programResult ->
            ( { model | program = programResult }
            , RemoteData.map imageRequests programResult
                |> RemoteData.withDefault Cmd.none
            )

        ProgramImagesRetrieved imageData ->
            ( { model
                | program = RemoteData.map (Program.addImage imageData) model.program
              }
            , Cmd.none
            )

        InformationRetrieved informationResult ->
            ( { model
                | information = informationResult
              }
            , Cmd.none
            )


imageRequests : Program.Program -> Cmd Msg
imageRequests (Program entries) =
    List.map
        (Requests.getImage ProgramImagesRetrieved)
        entries
        |> Cmd.batch
