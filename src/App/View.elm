module App.View exposing (view)

import App.Model exposing (Model)
import App.Update exposing (Msg)
import Html exposing (Html)
import Html.Attributes as Attributes
import RemoteData exposing (RemoteData(..), WebData)
import Types.Information as Information exposing (Information, InformationEntry)
import Types.Program.EntryType exposing (EntryType)
import Types.Program.Image as Image exposing (Image)
import Types.Program.Presenter as Presenter exposing (Presenter)
import Types.Program.Program as Program exposing (Program)
import Types.Program.ProgramEntry as ProgramEntry exposing (ProgramEntry)
import Types.Program.Summary as Summary exposing (Summary)
import Types.Program.Timeslot as Timeslot exposing (Timeslot)
import Types.Program.Title as ProgramTitle
import Types.Title as Title exposing (Title)


view : Model -> Html Msg
view model =
    Html.div
        [ Attributes.class "content" ]
        [ viewHeader
        , viewInformationBox model.information
        , viewProgramContent model.program
        ]


viewHeader : Html Msg
viewHeader =
    Html.div [ Attributes.class "header" ]
        [ Html.div [ Attributes.class "header-logo" ]
            [ Html.img
                [ Attributes.src "/ostkantkonferanse-ikon.png"
                , Attributes.class "banner"
                ]
                []
            , Html.span [ Attributes.class "banner-text" ] [ Html.text "stkantkonferansen" ]
            ]
        , Html.img
            [ Attributes.src "/banner.png"
            , Attributes.class "banner-big"
            ]
            []
        ]


viewInformationBox : WebData Information -> Html Msg
viewInformationBox informationData =
    case informationData of
        Success information ->
            viewInformationEntries information

        Failure e ->
            Html.text "Det skjedde en feil i innlasting av siden. Vennligst kontakt oss om dette på Facebook :)"

        _ ->
            Html.text "Laster..."


viewInformationEntries : Information -> Html Msg
viewInformationEntries information =
    Html.div []
        (Information.entries information
            |> divideListInListOfTwos
            |> List.map (\( first, second ) -> viewInformationRow first second)
        )


divideListInListOfTwos : List a -> List ( a, Maybe a )
divideListInListOfTwos theList =
    listDivideHelper [] (List.reverse theList)


listDivideHelper : List ( a, Maybe a ) -> List a -> List ( a, Maybe a )
listDivideHelper result original =
    case original of
        head :: head2 :: tail ->
            listDivideHelper (( head, Just head2 ) :: result) tail

        head :: [] ->
            ( head, Nothing ) :: result

        [] ->
            result


viewInformationRow : InformationEntry -> Maybe InformationEntry -> Html Msg
viewInformationRow firstColumn maybeSecondColumn =
    let
        firstContent : Html Msg
        firstContent =
            viewContentBox
                firstColumn.title
                (viewInformationContent firstColumn)

        secondContent : Html Msg
        secondContent =
            Maybe.map
                (\secondColumn ->
                    viewContentBox
                        secondColumn.title
                        (viewInformationContent secondColumn)
                )
                maybeSecondColumn
                |> Maybe.withDefault viewEmptyContentBox
    in
    viewDoubleContent firstContent secondContent


viewInformationContent : InformationEntry -> Html Msg
viewInformationContent entry =
    let
        row : Maybe String -> List (Html msg)
        row mString =
            Maybe.map (\content -> [ Html.text content, Html.br [] [] ]) mString
                |> Maybe.withDefault [ Html.br [] [] ]

        rows =
            [ entry.row1, entry.row2, entry.row3, entry.row4 ]
                |> trimNothings
                |> List.concatMap row
    in
    Html.p [] rows


trimNothings : List (Maybe a) -> List (Maybe a)
trimNothings maybes =
    maybes
        |> trimNothingsL
        |> List.reverse
        |> trimNothingsL
        |> List.reverse


trimNothingsL : List (Maybe a) -> List (Maybe a)
trimNothingsL maybes =
    case maybes of
        Nothing :: tail ->
            trimNothings tail

        _ ->
            maybes


viewConferenceDescription : Html Msg
viewConferenceDescription =
    viewContentBox (Title.title "hva") (Html.p [] [ Html.text "Østkantlagene i Oslo SV ønsker velkommen til Østkantkonferansen 2018, en konferanse om hvordan det er å vokse opp på østkanten i Oslo, og hva vi kan gjøre for å sikre en trygg og god oppvekst for alle." ])


viewLocation : Html Msg
viewLocation =
    viewContentBox
        (Title.title "hvor")
        (Html.p
            []
            [ Html.text "Popsenteret, Trondheimsveien 2, 0560 Oslo"
            , Html.br [] []
            , Html.br [] []
            , Html.text "9.6.2018 - 09:00"
            ]
        )


viewProgramContent : WebData Program.Program -> Html Msg
viewProgramContent programData =
    viewContentBox
        (Title.title "program")
        (case programData of
            Success program ->
                viewProgram program

            Failure error ->
                viewError

            _ ->
                viewLoading
        )


viewProgram : Program.Program -> Html Msg
viewProgram program =
    Html.div
        [ Attributes.class "program-box" ]
        (Program.sort program |> Program.map viewProgramEntry)


viewProgramEntry : ProgramEntry -> Html Msg
viewProgramEntry entry =
    Html.div
        [ Attributes.class "program-entry" ]
        [ viewProgramEntryHeader (ProgramEntry.beginTime entry) (ProgramEntry.endTime entry)
        , viewProgramEntryBody entry
        ]


viewProgramEntryHeader : Timeslot -> Timeslot -> Html Msg
viewProgramEntryHeader beginTime endTime =
    Html.div
        [ Attributes.class "program-entry-header" ]
        [ viewEntryHeaderContent beginTime endTime ]


viewEntryHeaderContent : Timeslot -> Timeslot -> Html Msg
viewEntryHeaderContent beginTime endTime =
    Html.div
        [ Attributes.class "program-entry-header-content" ]
        [ Html.p
            []
            [ Timeslot.icon beginTime ++ " " ++ Timeslot.toString beginTime ++ " - " ++ Timeslot.toString endTime |> Html.text ]
        ]


viewProgramEntryBody : ProgramEntry -> Html Msg
viewProgramEntryBody entry =
    Html.div
        [ Attributes.class "program-entry-body" ]
        [ viewProgramEntryBodyContent entry ]


viewProgramEntryBodyContent : ProgramEntry -> Html Msg
viewProgramEntryBodyContent entry =
    Html.div
        [ Attributes.class "program-entry-body-content" ]
        [ viewEntryIcon (ProgramEntry.entryType entry)
        , viewTitleAndSummary (ProgramEntry.title entry) (ProgramEntry.summary entry)
        , viewPresenter (ProgramEntry.presenter entry) (ProgramEntry.image entry)
        ]


viewEntryIcon : EntryType -> Html Msg
viewEntryIcon entryType =
    Html.text ""


viewTitleAndSummary : ProgramTitle.Title -> Maybe Summary -> Html Msg
viewTitleAndSummary title summary =
    Html.div
        [ Attributes.class "program-entry-body-title-and-summary" ]
        [ viewEntryContentTitle title
        , viewEntryContentSummary summary
        ]


viewEntryContentTitle : ProgramTitle.Title -> Html Msg
viewEntryContentTitle title =
    Html.h2
        []
        [ ProgramTitle.toString title |> Html.text ]


viewEntryContentSummary : Maybe Summary -> Html Msg
viewEntryContentSummary summary =
    Html.p
        [ Attributes.class "program-entry-body-summary" ]
        [ Maybe.map
            Summary.toString
            summary
            |> Maybe.withDefault ""
            |> Html.text
        ]


viewPresenter : Maybe Presenter -> WebData Image -> Html Msg
viewPresenter presenter image =
    Html.div
        [ Attributes.class "program-entry-body-presenter" ]
        [ viewPresenterImage image
        , viewPresenterName presenter
        ]


viewPresenterImage : WebData Image -> Html Msg
viewPresenterImage imageData =
    case imageData of
        Success image ->
            Html.img
                [ Attributes.class "program-entry-body-presenter-image"
                , Attributes.src (Image.url image)
                , Attributes.alt (Image.title image)
                ]
                []

        _ ->
            Html.text ""


viewPresenterName : Maybe Presenter -> Html Msg
viewPresenterName presenter =
    Html.p
        [ Attributes.class "program-entry-body-presenter-name" ]
        [ Maybe.map
            Presenter.toString
            presenter
            |> Maybe.withDefault ""
            |> Html.text
        ]


viewError : Html Msg
viewError =
    Html.h2 [] [ Html.text "Kunne ikke laste programmet. Kontakt Østkantkonferansen for å si fra om feilen" ]


viewLoading : Html Msg
viewLoading =
    Html.h2 [] [ Html.text "Laster program..." ]


viewContentBox : Title -> Html Msg -> Html Msg
viewContentBox title content =
    Html.div
        [ Attributes.class "content-box" ]
        [ Html.h1
            []
            [ title
                |> Title.toString
                |> String.toUpper
                |> Html.text
            ]
        , Html.div
            [ Attributes.class "content-box-content" ]
            [ content ]
        ]


viewEmptyContentBox : Html Msg
viewEmptyContentBox =
    Html.div
        [ Attributes.class "content-box" ]
        [ Html.h1
            []
            []
        , Html.div
            [ Attributes.class "content-box-content" ]
            []
        ]


viewDoubleContent : Html Msg -> Html Msg -> Html Msg
viewDoubleContent first second =
    Html.div
        [ Attributes.class "double-content" ]
        [ first
        , second
        ]
