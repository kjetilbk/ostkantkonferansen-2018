module App.Model exposing (Model)

import RemoteData exposing (WebData)
import Types.Information exposing (Information)
import Types.Program.Program as Program


type alias Model =
    { program : WebData Program.Program
    , information : WebData Information
    }
