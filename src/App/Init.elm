module App.Init exposing (init)

import App.Model exposing (Model)
import App.Update exposing (Msg(..))
import RemoteData
import Requests


init : ( Model, Cmd Msg )
init =
    ( initModel
    , Cmd.batch [ Requests.getProgram ProgramRetrieved, Requests.getInformation InformationRetrieved ]
    )


initModel : Model
initModel =
    { program = RemoteData.Loading
    , information = RemoteData.Loading
    }
